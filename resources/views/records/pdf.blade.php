<style type="text/css" media="all">
    table{
        width: 100%;
        font-family: "Courier New", Courier, monospace;
        font-size: 9px;
        border-collapse: collapse;

    }
    thead{
        border-top: 1px dashed #000;
    }
    td{
        text-align: left;
        padding:5px;
    }
    th{
        text-align: left;
        padding-top:10px;
    }
</style>
{{--<table>
<tr>
    <td></td>
    <td><img src="{{asset('/png/notjustok.png')}}" width="500"></td>
    <td>
        Not Just OK<br>
        Lekki<br>
        Lagos<br>
        Nigeria
    </td>
</tr>
</table>--}}
<table class="table">
    <thead>
    <tr>
        <td>#</td>
        <td>ISRC/Catalog</td>
        <td>Format</td>
        <td>Territory</td>
        <td>Sales Channel</td>
        <td>Sales Source</td>
        <td>Units</td>
        <td>Sales Value</td>
        <td>Rate Payable</td>
        <td>Contract Perc.</td>
        <td>Pro Rata Share %</td>
        <td>Amount Payable</td>
    </tr>
    </thead>
    <tbody>
    @foreach($records as $record)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$record->isrc_code}}</td>
            <td>Single</td>
            <td>{{$record->territory_name}}</td>
            <td>{{$record->sales_channel}}</td>
            <td>{{$record->sales_source}}</td>
            <td>{{$record->units}}</td>
            <td>{{$record->value_converted}}</td>
            <td>{{$record->contract}}</td>
            <td>{{$record->rate_payable}}</td>
            <td>{{$record->pro_rata_share}}</td>
            <td>{{$record->calculated_royalty}}</td>
        </tr>
    @endforeach
    </tbody>
</table>