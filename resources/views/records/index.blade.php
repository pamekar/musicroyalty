@extends('layouts.app')
@section('title','Records')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap4.css">
    <link rel="stylesheet" href="/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="/css/responsive.bootstrap4.min.css">
    <script>
        const ajaxURL = "{{route('records.json')}}";
        const columns = [
            {name: 'reporting_region'},
            {name: 'start_date'},
            {name: 'end_date'},
            {name: 'upc'},
            {name: 'grid'},
            {name: 'isrc'},
            {name: 'custom_id_1'},
            {name: 'custom_id_2'},
            {name: 'custom_id_3'},
            {name: 'custom_id_4'},
            {name: 'google_id'},
            {name: 'artist'},
            {name: 'product_title'},
            {name: 'container_title'},
            {name: 'content_provider'},
            {name: 'label'},
            {name: 'consumer_country'},
            {name: 'device_type'},
            {name: 'product_type'},
            {name: 'interaction_type'},
            {name: 'count'},
            {name: 'total_plays'},
            {name: 'partner_revenue_paid'},
            {name: 'partner_revenue_currency'},
            {name: 'eur_amount'}
        ];
        const serverSide = true;
    </script>
@endsection
@section('content')
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Records</h3>
        </div>
        <div class="block-content block-content-full">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons display responsive">
                <thead>
                <tr>
                    <th>Reporting_Region</th>
                    <th>Start_Date</th>
                    <th>End_Date</th>
                    <th>UPC</th>
                    <th>GRID</th>
                    <th>ISRC</th>
                    <th>Custom_ID_1</th>
                    <th>Custom_ID_2</th>
                    <th>Custom_ID_3</th>
                    <th>Custom_ID_4</th>
                    <th>Google_ID</th>
                    <th>Artist</th>
                    <th>Product_Title</th>
                    <th>Container_Title</th>
                    <th>Content_Provider</th>
                    <th>Label</th>
                    <th>Consumer_Country</th>
                    <th>Device_Type</th>
                    <th>Product_Type</th>
                    <th>Interaction_Type</th>
                    <th>Count</th>
                    <th>Total_Plays</th>
                    <th>Partner_Revenue_Paid</th>
                    <th>Partner_Revenue_Currency</th>
                    <th>EUR_Amount</th>

                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/jquery.datatables.min.js"></script>
    <script src="/js/datatables.bootstrap4.min.js"></script>
    <script src="/js/datatables.buttons.min.js"></script>
    <script src="/js/buttons.print.min.js"></script>
    <script src="/js/pdfmake.min.js"></script>
    <script src="/js/vfs_fonts.js"></script>
    <script src="/js/buttons.html5.min.js"></script>
    <script src="/js/buttons.flash.min.js"></script>
    <script src="/js/buttons.colvis.min.js"></script>
    <script src="/js/dataTables.responsive.min.js"></script>
    <script src="/js/responsive.bootstrap4.min.js"></script>
    <script src="/js/be_tables_datatables.min.js"></script>
@endsection