<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    //
    protected $fillable
        = [
            'user_id',
            'upload_id',
            'reporting_region',
            'start_date',
            'end_date',
            'upc',
            'grid',
            'isrc',
            'custom_id_1',
            'custom_id_2',
            'custom_id_3',
            'custom_id_4',
            'google_id',
            'artist',
            'product_title',
            'container_title',
            'sales_source',
            'content_provider',
            'label',
            'consumer_country',
            'device_type',
            'product_type',
            'interaction_type',
            'count',
            'total_plays',
            'partner_revenue_paid',
            'partner_revenue_currency',
            'eur_amount',
        ];

    protected $appends
        = [
            'company_code',
            'company_name',
            'payee_code',
            'payee_name',
            'account_code',
            'account_name',
            'sales_reference',
            'catalog_code',
            'format',
            'artist_name',
            'track_title',
            'isrc_code',
            'track_code',
            'territory_code',
            'territory_name',
            'price_line',
            'dist_channel_code',
            'sales_channel',
            'value_converted',
            'rate_type',
            'rate_payable',
            'pro_rata_share',
            'contract',
            'sales',
            'units',
            'calculated_royalty',
        ];

    public function upload()
    {
        return $this->belongsTo('App\Upload');
    }

    public function getCompanyCodeAttribute()
    {
        return "NJO";
    }

    public function getCompanyNameAttribute()
    {
        return "Not Just OK";
    }

    public function getPayeeCodeAttribute()
    {
        return strtoupper(substr($this->label, 0, 3));
    }

    public function getPayeeNameAttribute()
    {
        return $this->label;
    }

    public function getAccountCodeAttribute()
    {
        return substr($this->label, 0, 3);
    }

    public function getAccountNameAttribute()
    {
        return $this->label;
    }

    public function getSalesReferenceAttribute()
    {
        return $this->isrc;
    }

    public function getCatalogCodeAttribute()
    {
        return null;
    }

    public function getFormatAttribute()
    {
        return "SING";
    }

    public function getArtistNameAttribute()
    {
        return $this->artist;
    }

    public function getTrackTitleAttribute()
    {
        return $this->container_title;
    }

    public function getIsrcCodeAttribute()
    {
        return $this->isrc;
    }

    public function getTrackCodeAttribute()
    {
        return 1142;
    }

    public function getTerritoryCodeAttribute()
    {
        return $this->reporting_region;
    }

    public function getTerritoryNameAttribute()
    {
        return __("app.countries.$this->reporting_region");
    }

    public function getPriceLineAttribute()
    {
        return "Full Price";
    }

    public function getDistChannelCodeAttribute()
    {
        return $this->sales_source === "iTunes" ? "DOWN" : "STRE";
    }

    public function getSalesChannelAttribute()
    {
        return $this->sales_source === "iTunes" ? "Download" : "Stream";

    }

    public function getValueConvertedAttribute()
    {
        return $this->eur_amount / 1.17;
    }

    public function getRateTypeAttribute()
    {
        return "Receipts";
    }

    public function getRatePayableAttribute()
    {
        return 70;
    }

    public function getProRataShareAttribute()
    {
        return 100;
    }

    public function getContractAttribute()
    {
        return 100;
    }

    public function getSalesAttribute()
    {
        return 100;
    }

    public function getUnitsAttribute()
    {
        return $this->count;
    }

    public function getCalculatedRoyaltyAttribute()
    {
        return $this->eur_amount / 1.17 * 0.7;
    }

}
