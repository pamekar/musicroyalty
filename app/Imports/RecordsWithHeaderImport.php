<?php

namespace App\Imports;

use App\Record;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

/**
 * Class RecordsWithHeaderImport
 * drg >> Add records from excel rows with header
 *
 * @package App\Imports
 */
class RecordsWithHeaderImport implements ToModel, WithHeadingRow
{
    private $rows = 0;
    private $uploadId = 0;
    private $sales_source = "";

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        ++$this->rows;
        return new Record([
            // drg >> add rows to DB
            'upload_id'                => $this->uploadId,
            'user_id'                  => Auth::id(),
            'reporting_region'         => $row['reporting_region'],
            'start_date'               => $row['start_date'],
            'end_date'                 => $row['end_date'],
            'upc'                      => $row['upc'],
            'grid'                     => $row['grid'],
            'isrc'                     => $row['isrc'],
            'custom_id_1'              => $row['custom_id_1'],
            'custom_id_2'              => $row['custom_id_2'],
            'custom_id_3'              => $row['custom_id_3'],
            'custom_id_4'              => $row['custom_id_4'],
            'google_id'                => $row['google_id'],
            'artist'                   => $row['artist'],
            'product_title'            => $row['product_title'],
            'container_title'          => $row['container_title'],
            'content_provider'         => $row['content_provider'],
            'label'                    => $row['label'],
            'consumer_country'         => $row['consumer_country'],
            'sales_source'             => $this->sales_source,
            'device_type'              => $row['device_type'],
            'product_type'             => $row['product_type'],
            'interaction_type'         => $row['interaction_type'],
            'count'                    => $row['count'],
            'total_plays'              => $row['total_plays'],
            'partner_revenue_paid'     => $row['partner_revenue_paid'],
            'partner_revenue_currency' => $row['partner_revenue_currency'],
            'eur_amount'               => $row['eur_amount'],

        ]);
    }


    public function getRowCount()
    {
        return $this->rows;
    }

    public function setUpload($upload)
    {
        $this->uploadId = $upload->id;
        $this->sales_source= $upload->sales_source;
    }
}
