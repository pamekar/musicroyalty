<?php

namespace App\Http\Controllers;

use App\Imports\RecordsImport;
use App\Imports\RecordsWithHeaderImport;
use App\Record;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */

    public function index()
    {
        //
        return view('records.index');
    }

    /**
     * Returns list of records via json
     *
     * @return array
     */
    public function json()
    {
        //
        return Laratables::recordsOf(Record::class, function ($query) {
            return $query->where('user_id', Auth::id());
        });
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refined()
    {
        return view('records.refined');
    }

    /**
     * @return mixed
     */
    public function csv(Request $request)
    {
        $columns = [
            'isrc',
            'label',
            'artist',
            'product_title',
            'country',
            'start_at',
            'end_at'
        ];
        $download = Record::where(function ($query) use ($columns, $request) {
            foreach ($columns as $column) {
                $request->$column !== null ? $query->whereIn($column,
                    explode(';', $request->$column)) : null;
            }
        })->orderBy($request->order ?: 'id')->get();
        return response()->json($download);
    }

    public function download(Request $request, $type)
    {
        return $type === "csv" ? $this->csv($request)
            : ($type === "pdf" ? $this->pdf($request) : null);
    }

    /**
     * @return mixed
     */
    public function pdf(Request $request)
    {
        $columns = [
            'isrc',
            'label',
            'artist',
            'product_title',
            'country',
            'start_at',
            'end_at'
        ];

        $records = Record::where(function ($query) use ($columns, $request) {
            foreach ($columns as $column) {
                $request->$column !== null ? $query->whereIn($column,
                    explode(';', $request->$column)) : null;
            }
        })->orderBy($request->order ?: 'id')->limit(300 )->get();

        $pdf = App::make('dompdf.wrapper');

        $pdf->loadView('records.pdf', compact('records'));
        //return view('records.pdf', compact('records'));
        return $pdf->setPaper('a4', 'landscape')->download('records.pdf');

    }

    /**
     * @return mixed
     */
    public function search(Request $request)
    {
        $columns = [
            'isrc',
            'label',
            'artist',
            'product_title',
            'consumer_country',
            'start_at',
            'end_at'
        ];

        return Record::where(function ($query) use ($columns, $request) {
            foreach ($columns as $column) {
                $request->$column !== null ? $query->whereIn($column,
                    explode(';', $request->$column)) : null;
            }
        })->orderBy($request->order ?: 'id')->paginate(20);

    }

    public function options($column)
    {
        if (!in_array($column, [
            'isrc',
            'label',
            'artist',
            'product_title',
            'consumer_country',
            'start_at',
            'end_at'
        ])
        ) {
            abort(404);
        }
        return response()->json(Record::where($column, '<>', null)->distinct()
            ->orderBy($column)
            ->pluck($column));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $upload)
    {
        //
        $records = $request->first_row_header ? new RecordsWithHeaderImport()
            : new RecordsImport();
        $records->setUpload($upload);
        Excel::import($records, $request->file('records'));

        $rowCount = $records->getRowCount();

        $upload->row_count = $rowCount;
        $upload->save();

        return redirect('uploads')
            ->with('notification', [
                'type'    => 'success',
                'message' => "Our system accepted your data. Congrats, $rowCount rows have been added to your records. :)"
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Record $record
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Record $record)
    {
        //
        return view('records.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Record $record
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Record $record)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Record              $record
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Record $record)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Record $record
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Record $record)
    {
        //
    }
}
