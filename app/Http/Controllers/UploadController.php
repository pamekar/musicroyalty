<?php

namespace App\Http\Controllers;

use App\Upload;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('uploads.index');
    }

    /**
     * Returns list of records via json
     *
     * @return array
     */
    public function json()
    {
        //
        return Laratables::recordsOf(Upload::class, function ($query) {
            return $query->where('user_id', Auth::id());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('uploads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if ($request->hasFile('records')) {
            $upload = Upload::create([
                'user_id'     => Auth::id(),
                'sales_source'       => $request->sales_source,
                'batch_code'  => "rec-" . date('ymdHis'),
                'description' => $request->description
            ]);

            $record = new RecordController();

            return $record->store($request, $upload);
        }
        return abort(500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Upload $upload
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Upload $upload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Upload $upload
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Upload $upload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Upload              $upload
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Upload $upload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Upload $upload
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Upload $upload)
    {
        //
    }
}
